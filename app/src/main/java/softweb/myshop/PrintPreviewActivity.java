package softweb.myshop;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.print.PrintManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import softweb.printinginvoice.CustomPrintDocumentAdapter;

public class PrintPreviewActivity extends AppCompatActivity
{
    Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_preview);

        byte[] b = getIntent().getByteArrayExtra("byte");
        bitmap = BitmapFactory.decodeByteArray(b,0,b.length);

        ImageView imageView = (ImageView) findViewById(R.id.print_preview);
        imageView.setImageBitmap(bitmap);

        Button print = (Button)findViewById(R.id.print);
        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doPrint();
            }
        });
    }

    private void doPrint()
    {
        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
        printManager.print("hello world",
                new CustomPrintDocumentAdapter(this,bitmap),
                null
        );
    }
}
