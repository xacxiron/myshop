package softweb.myshop;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Jobayed Ullah on 11/13/2016.
 */

public class Sell
{
    private static int invoiceCounter = 1;

    private Product _product;
    private String  _invoice;
    private String  _executionTime;


    //Constructor
    public Sell(Product product)
    {
        this._product                                = product;
        android.text.format.DateFormat dateFormat    = new android.text.format.DateFormat();
        this._executionTime                          = dateFormat.format("hh:mm aa dd-MM-yyyy", new Date()).toString();
        this._invoice                                = ""+Sell.invoiceCounter++;

    }

    public Sell(Sell sell)
    {
        this._product        = sell.get_product();
        this._executionTime  = sell.get_executionTime();
        this._invoice        = sell.get_invoice();
    }






    //Getters
    public Product get_product() {
        return _product;
    }

    public String get_invoice() {
        return _invoice;
    }

    public String get_executionTime() {
        return _executionTime;
    }
}
