package softweb.myshop;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static java.security.AccessController.getContext;

public class ProductListActivity extends AppCompatActivity
{
    Singleton s = Singleton.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.prod_list_title);
        setContentView(R.layout.activity_product_list);

        ArrayList<Product> totalProduct = new ArrayList<Product>();
        totalProduct = s.getAllProducts(this);

        final GridView gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new ImageAdapter(this, totalProduct));
        LayoutAnimationController lac = new LayoutAnimationController(AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.gv_animation), 0.25f); //0.25f == time between appearance of listview items.
        gridView.setLayoutAnimation(lac);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                String primaryKey = ((TextView)view.findViewById(R.id.text_model)).getText().toString();



                Intent intent = new Intent(getApplicationContext(),SingleProductView.class);
                intent.putExtra("PrimaryKey",primaryKey);
                startActivity(intent);
            }
        });


    }



    ///Adapter
    public class ImageAdapter extends BaseAdapter
    {
        private Context mContext;
        private LayoutInflater inflater;
        ArrayList<Product> totalProduct = new ArrayList<Product>();





        //Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.gv_animation);


        public ImageAdapter(Context c,ArrayList<Product> totalProduct)
        {

            mContext = c;
            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.totalProduct = totalProduct;

        }

        public int getCount() {
            return totalProduct.size();
        }
        public Object getItem(int position) {
            return totalProduct.get(position);
        }
        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent)
        {
            View gridView;

            if(convertView == null)
            {
                gridView = inflater.inflate(R.layout.gv_layout, null);

                Typeface light = s.getFontFromRes(mContext, R.raw.light);
                // set value into textview
                TextView textCount = (TextView) gridView.findViewById(R.id.txt_count);
                textCount.setText(totalProduct.get(position).getQuantity()+"");
                textCount.setTypeface(light);

                TextView textTitle = (TextView) gridView.findViewById(R.id.text_title);
                textTitle.setText(totalProduct.get(position).getName());
                textTitle.setTypeface(light);

                TextView textModel = (TextView) gridView.findViewById(R.id.text_model);
                textModel.setText(totalProduct.get(position).getModel());
                textModel.setTypeface(light);

                TextView textPrice = (TextView) gridView.findViewById(R.id.text_price);
                textPrice.setText("৳"+totalProduct.get(position).getPrice());
                textPrice.setTypeface(light);

                // set image based on selected text
                ImageView imageView = (ImageView) gridView.findViewById(R.id.img_icon);
                imageView.setImageResource(R.drawable.ic_add2);
            }
            else gridView = convertView;

            //gridView.startAnimation(animation);
            return gridView;
        }



    }///Adapter ends here


}
