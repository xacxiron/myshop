package softweb.myshop;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Jahid on 11/1/2016.
 */

public class Singleton
{



    public ArrayList<Sell> sells = new ArrayList<Sell>();


    /*create an object of SingleObject*/
    private static Singleton instance = new Singleton();

    private Singleton(){}

    /*Get the only object available*/
    public static Singleton getInstance()
    {
        return instance;
    }


    /*
     * Added by Jobayed Ullah on 09/11/2016
     *
     * This public method is to populate Product table data in an Arraylist
     * it takes an object of type 'Product' class as argument
     * and returns an Arraylist.
     */
    public ArrayList<Product> getAllProducts(Context context)
    {
        DatabaseOperation databaseOperation = new DatabaseOperation(context);
        Cursor cursor = databaseOperation.getProductInformation(databaseOperation);

        ArrayList<Product> p = new ArrayList<Product>();
        if (cursor.getCount()>0)
        {
            while (cursor.moveToNext())
            {
                Product product = new Product
                    (
                        cursor.getString(cursor.getColumnIndex(databaseOperation.productName)),
                        cursor.getString(cursor.getColumnIndex(databaseOperation.productModel)),
                        cursor.getInt(cursor.getColumnIndex(databaseOperation.productQuantity)),
                        cursor.getDouble(cursor.getColumnIndex(databaseOperation.productPrice))
                    );
                p.add(product);
            }
        }
        return p;
    }

    /*
     * Added by Jobayed Ullah on 09/11/2016
     *
     * This public method is to populate Product table data in an Arraylist
     * it takes an object of type 'Product' class as argument
     * and returns an Arraylist.
     */
    public Product getSingleProduct(Context context,String pKey)
    {
        DatabaseOperation databaseOperation = new DatabaseOperation(context);
        Cursor cursor = databaseOperation.getSingleProductInformation(databaseOperation,pKey);

        Product product = null;
        if (cursor.getCount()>0)
        {
            while (cursor.moveToNext())
            product = new Product(

                    cursor.getString(cursor.getColumnIndex(databaseOperation.productName)),
                    cursor.getString(cursor.getColumnIndex(databaseOperation.productModel)),
                    cursor.getInt(cursor.getColumnIndex(databaseOperation.productQuantity)),
                    cursor.getDouble(cursor.getColumnIndex(databaseOperation.productPrice))
            );

            Log.d("item",product.getName()+"\n"+product.getModel()+"\n");

        }
        return product;
    }

    /*
     * this function adds a typeface from the raw resource .ttf file
     * Paremeter 2( context of the activity, resource identifier)
     */
    public Typeface getFontFromRes(Context context,int resource)
    {
        Typeface tf = null;
        InputStream is = null;
        try {
            is = context.getResources().openRawResource(resource);
        }
        catch(Resources.NotFoundException e) {
            Log.e("TAG:", "Could not find font in resources!");
        }

        String outPath = context.getCacheDir() + "/tmp" + System.currentTimeMillis() +".raw";

        try
        {
            byte[] buffer = new byte[is.available()];
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outPath));

            int l = 0;
            while((l = is.read(buffer)) > 0)
                bos.write(buffer, 0, l);

            bos.close();

            tf = Typeface.createFromFile(outPath);

            // clean up
            new File(outPath).delete();
        }
        catch (IOException e)
        {
            Log.e("TAG", "Error reading in font!");
            return null;
        }

        Log.d("TAG", "Successfully loaded font.");

        return tf;
    }
}
