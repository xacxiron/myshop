package softweb.myshop;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import BarcodeScanner.IntentIntegrator;
import BarcodeScanner.IntentResult;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{

    String barcodeType,barcodeContent;
    Cursor cursor;

    int count=0;

    Button ok,addButton,scanBarcode;

    Singleton singleton = Singleton.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ok = (Button) findViewById(R.id.btn_ok);
        addButton = (Button) findViewById(R.id.btn_add);
        scanBarcode = (Button) findViewById(R.id.scanCode);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(count == 5){
            addButton.setVisibility(View.GONE);
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //By this floating Action Button, Add product Activity gets called
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent intentAddProduct = new Intent(getApplicationContext(),AddProduct.class);
                startActivity(intentAddProduct);
            }
        });


        ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {

                if(isAvailable())
                {
                    Sell sell = new Sell(sellProduct());
                    if(sell.get_product().getQuantity()==1) deleteProduct();
                    else updateProductQuantity(sell.get_product().getQuantity()-1);
                    sell.get_product().setQuantity(1);
                    singleton.sells.add(sell);

                    Toast.makeText(getApplicationContext(),"no error",Toast.LENGTH_LONG).show();
                    Intent intent_sell_info = new Intent(getApplicationContext(), Sellinfo.class);
                    startActivity(intent_sell_info);
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ok.setVisibility(View.GONE);
        addButton.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id==R.id.menu_add_new_product){
            Intent intent=new Intent(this,AddProduct.class);
            startActivity(intent);
        }
        else if(id==R.id.menu_all_products){
            Intent intent=new Intent(this,ProductListActivity.class);
            startActivity(intent);
        }
        else if(id==R.id.menu_sell_info){
            //Intent intent=new Intent(this,AddProduct.class);
            //startActivity(intent);
            Toast.makeText(getApplicationContext(), "Will implement soon !!!", Toast.LENGTH_SHORT).show();
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_add)
        {

        }
        else if (id == R.id.nav_edit)
        {

        }
        else if (id == R.id.nav_delete)
        {

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void scanProduct(View view){
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.initiateScan();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            barcodeContent = scanningResult.getContents();
            barcodeType=scanningResult.getFormatName();
            if(!isAvailable()){
                Toast.makeText(getApplicationContext(),"No products available",Toast.LENGTH_LONG).show();
            }
        }
        else Toast.makeText(getApplicationContext(), "No scan data received!", Toast.LENGTH_SHORT).show();

        addButton.setVisibility(View.VISIBLE);
        ok.setVisibility(View.VISIBLE);
        scanBarcode.setVisibility(View.GONE);
    }

    public Sell sellProduct()
    {
        DatabaseOperation databaseOperation = new DatabaseOperation(this);
        cursor.moveToFirst();

        Sell sell = new Sell(new Product(
                cursor.getString(0),
                cursor.getString(1),
                Integer.parseInt(cursor.getString(2)),
                Double.parseDouble(cursor.getString(3))
        ));

        databaseOperation.salesTableInsert(
                databaseOperation,
                sell.get_product().getName() ,
                sell.get_product().getModel(),
                barcodeType,
                barcodeContent,
                sell.get_product().getPrice(),
                sell.get_executionTime(),
                null,
                null
        );
        return sell;
    }

    public boolean isAvailable()
    {
        DatabaseOperation databaseOperation = new DatabaseOperation(this);
        cursor=databaseOperation.searchProduct(databaseOperation,barcodeType,barcodeContent);
        return cursor.getCount() > 0;
    }

    public void updateProductQuantity(int quantity)
    {
        DatabaseOperation databaseOperation = new DatabaseOperation(this);
        databaseOperation.updateQuantity(databaseOperation,barcodeType,barcodeContent,quantity);
    }

    public void deleteProduct()
    {
        DatabaseOperation databaseOperation = new DatabaseOperation(this);
        databaseOperation.deleteproduct(databaseOperation,barcodeType,barcodeContent);
    }

    public void addMoreProduct(View view)
    {
        scanProduct(view);
        if(isAvailable())
        {
            Sell sell = new Sell(sellProduct());
            if(sell.get_product().getQuantity()==1) deleteProduct();
            else updateProductQuantity(sell.get_product().getQuantity()-1);
            sell.get_product().setQuantity(1);
            singleton.sells.add(sell);
        }
    }
}
