package softweb.myshop;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SingleProductView extends AppCompatActivity
{
    Singleton s = Singleton.getInstance();
    Product product ;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_product_view);


        //Set Title from parent Activity
        Intent intent = getIntent();
        String pKey = intent.getExtras().getString("PrimaryKey");
        product = new Product(s.getSingleProduct(this,pKey));
        //setTitle(product.getName());

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle(product.getName().toUpperCase());
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this,R.color.colorAccent));

        Typeface bold = s.getFontFromRes(this,R.raw.bold);
        Typeface semiBold = s.getFontFromRes(this,R.raw.semibold);
        Typeface light = s.getFontFromRes(this,R.raw.light);

        collapsingToolbarLayout.setCollapsedTitleTypeface(semiBold);
        collapsingToolbarLayout.setExpandedTitleTypeface(bold);


        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(fabOCL);



        TextView tvModel = (TextView)findViewById(R.id.tv_modelSingle);
        tvModel.setTypeface(semiBold);
        tvModel.setText("Model : "+product.getModel());

        TextView tvQuantity = (TextView)findViewById(R.id.tv_quantitySingle);
        tvQuantity.setTypeface(light);
        tvQuantity.setText(product.getQuantity()+" items available only.");

        TextView tvPrice = (TextView)findViewById(R.id.tv_priceSingle);
        tvPrice.setTypeface(light);
        tvPrice.setText(product.getPrice()+" tk.");
    }

    View.OnClickListener fabOCL = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    };




}
