package softweb.myshop;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import BarcodeScanner.IntentIntegrator;
import BarcodeScanner.IntentResult;

public class AddProduct extends AppCompatActivity
{
    EditText model,name,qty,price;
    Button add;
    String productName;
    String productModel;
    int quantity;
    double unitPrice;
    String barcodeData;
    String barcodeType;
    Cursor product;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        model = (EditText)findViewById(R.id.product_model_edit_text);
        name = (EditText)findViewById(R.id.product_name_edit_text);
        qty = (EditText)findViewById(R.id.product_quantity_edit_text);
        price = (EditText)findViewById(R.id.price_edit_text);
        add= (Button) findViewById(R.id.add_button);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProduct();
            }
        });
    }

    public void addProduct()
    {
        try {
            productName=name.getText().toString();
            productModel=model.getText().toString();
            quantity=Integer.parseInt(qty.getText().toString());
            unitPrice=Double.parseDouble(price.getText().toString());
            DatabaseOperation databaseOperation=new DatabaseOperation(getApplicationContext());
            if(isAvailable()){              //checks whether the product is already in stock.... if exists then we will update the product info
                int remainingQuantity= Integer.parseInt(product.getString(2));
                quantity = quantity + remainingQuantity;           //existing quantity + new quantity from the edit text field
                databaseOperation.updateProductTable(databaseOperation,productName,quantity,unitPrice,barcodeType,barcodeData);
                Toast.makeText(getApplicationContext(),"Product Table updated "+quantity,Toast.LENGTH_LONG).show();
            }else {     //if the product is not available in stock then we will simply add a new product
                databaseOperation.productTableInsert(databaseOperation, productName, productModel, quantity, unitPrice, barcodeType, barcodeData);
                Toast.makeText(getApplicationContext(),"New product added",Toast.LENGTH_LONG).show();
            }
        } catch (NumberFormatException e)
        {
            e.printStackTrace();
        }
    }

    public void scanBarcode(View view){
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator.initiateScan();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            barcodeData = scanningResult.getContents();
            barcodeType = scanningResult.getFormatName();
        }else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public boolean isAvailable(){//search for the product using the barcode of the product
        DatabaseOperation databaseOperation = new DatabaseOperation(this);
        product=databaseOperation.searchProduct(databaseOperation,barcodeType,barcodeData);
        product.moveToFirst();
        if(product.getCount()>0){
            return true;
        }else{
            return false;
        }
    }
}



