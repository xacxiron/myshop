package softweb.myshop;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by jahid on 10/30/2016.
 */

public class DatabaseOperation extends SQLiteOpenHelper
{

    public static final String databaseName="My_shop";
    public static final int databaseVersion=1;
    public final String productTableName="product_table";
    public final String salesTableName="sales_table";
    public final String productName="name";
    public final String productModel="Model";
    public final String productQuantity="Qty";
    public final String productPrice="Price";
    public final String productBarcodeData="Barcode";
    public final String productBarcodeType="Barcode_type";
    public final String invoiceId="Invoice_ID";
    public final String salesTime="Date_time";
    public final String customerName="Customer_name";
    public final String customerPhone="Phone";

    public String createProductTableSql="create table "+ productTableName+"(" +
            productName+" text,"+ productModel+" text primary key,"+
            productQuantity+" int,"+
            productPrice+" num,"+
            productBarcodeType+" text,"+
            productBarcodeData+" text);";

    public String createSalesTableSql="create table "+ salesTableName+"("+
            invoiceId+" text primary key,"+
            productName+" text,"+
            productModel+" text,"+
            productBarcodeType+" text,"+
            productBarcodeData+" text,"+
            productPrice+" num,"+
            salesTime+" text,"+
            customerName+" text,"+
            customerPhone+" text);";
    
    
    

    public DatabaseOperation(Context context)
    {
        super(context, databaseName, null, databaseVersion);
        Log.d("database operation","Database Created");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(createProductTableSql);
        Log.d("database operation","Product Table Created");
        sqLiteDatabase.execSQL(createSalesTableSql);
        Log.d("database operation","Sales Table Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void productTableInsert(DatabaseOperation databaseOperation,String name,String model,int quantity,double price,String barcodeType,String barcode){
        SQLiteDatabase database=databaseOperation.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(productName,name);
        contentValues.put(productModel,model);
        contentValues.put(productQuantity,quantity);
        contentValues.put(productPrice,price);
        contentValues.put(productBarcodeType,barcodeType);
        contentValues.put(productBarcodeData,barcode);
        long k=database.insert(productTableName,null,contentValues);
        Log.d("database operation","Data inserted");
    }
//..............method to insert data in sales table....................
    public void salesTableInsert(DatabaseOperation databaseOperation,String productName,String model,String barcodeType,String barcode,double price,String dateTime,String name,String phone){
        SQLiteDatabase database=databaseOperation.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(productName,productName);
        contentValues.put(productModel,model);
        contentValues.put(productBarcodeType,barcodeType);
        contentValues.put(productBarcodeData,barcode);
        contentValues.put(productPrice,price);
        contentValues.put(salesTime,dateTime);
        contentValues.put(customerName,name);
        contentValues.put(customerPhone,phone);
        long k=database.insert(salesTableName,null,contentValues);
        Log.d("database operation","Data inserted");
    }
    public Cursor searchProduct(DatabaseOperation databaseOperation,String barcodeType,String barcodeContent){
        Cursor product;
        SQLiteDatabase database=databaseOperation.getWritableDatabase();
        String[] seletedColumns={productName,productModel,productQuantity,productPrice};
        String selectionparameter=productBarcodeType+" like ? and "+productBarcodeData+" like ?";
        String[] selectionArguments={barcodeType,barcodeContent};
        product = database.query(productTableName,seletedColumns,selectionparameter,selectionArguments,null,null,null);
        return product;
    }

    public void updateQuantity(DatabaseOperation databaseOperation,String barcodeType,String barcodeContent,int quantity){
        SQLiteDatabase database = databaseOperation.getWritableDatabase();
        String selection = productBarcodeType+" like ? and "+productBarcodeData+" like ?";
        String[] args = {barcodeType,barcodeContent};
        ContentValues contentValues = new ContentValues();
        contentValues.put(productQuantity,quantity);
        database.update(productTableName,contentValues,selection,args);
    }

    public void deleteproduct(DatabaseOperation databaseOperation,String barcodeType,String barcodeContent){
        SQLiteDatabase database=databaseOperation.getWritableDatabase();
        String selection = productBarcodeType+" like ? and "+productBarcodeData+" like ?";
        String[] args = {barcodeType,barcodeContent};
        database.delete(productTableName,selection,args);
    }

    public Cursor getSellsInformation(DatabaseOperation databaseOperation){
        Cursor products;
        SQLiteDatabase database=databaseOperation.getWritableDatabase();
        String[] columns={productName,productModel,productPrice};
        products=database.query(salesTableName,columns,null,null,null,null,null);
        return products;
    }

    public void updateProductTable(DatabaseOperation databaseOperation,
                                   String productName,
                                   int quantity,
                                   double price,
                                   String barcodeType,
                                   String barcodeContent)
    {
        SQLiteDatabase database = databaseOperation.getWritableDatabase();
        String selection = productBarcodeType+" like ? and "+productBarcodeData+" like ?";
        String[] args = {barcodeType,barcodeContent};
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.productName,productName);
        contentValues.put(productQuantity,quantity);
        contentValues.put(productPrice,price);
        database.update(productTableName,contentValues,selection,args);
    }

    /*
     * this is a raw database query which returns all the rows of the table "productTableName"
     * Added by "Jobayed Ullah"
     * this method returns a cursor
     */
    public Cursor getProductInformation(DatabaseOperation databaseOperation)
    {
        String query = "SELECT * FROM "+ productTableName;
        SQLiteDatabase database=databaseOperation.getWritableDatabase();
        Cursor  cursor = database.rawQuery(query,null);
        return  cursor;
    }


    /*
     * this is a raw database query which returns a single row from the table "productTableName"
     * based on it's primary key 'productModel'
     *
     * Added by "Jobayed Ullah"
     * this method returns a cursor
     */
    public Cursor getSingleProductInformation(DatabaseOperation databaseOperation,String pKey)
    {
        String query = "SELECT * FROM "+ productTableName+" WHERE "+productModel+" = ?";
        String[] params = {pKey};

        SQLiteDatabase database=databaseOperation.getWritableDatabase();
        Cursor  cursor = database.rawQuery(query,params);
        return  cursor;
    }
}