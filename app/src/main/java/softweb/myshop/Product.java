package softweb.myshop;

/**
 * Created by Jobayed Ullah on 11/9/2016.
 */

public class Product
{
    private String name;
    private String model;
    private int    quantity;
    private double price;

    public Product(String name,String model,int quantity,double price)
    {
        this.name     = name;
        this.model    = model;
        this.quantity = quantity;
        this.price    = price;
    }

    public Product(String name,String model,double price)
    {
        this.name     = name;
        this.model    = model;
        this.quantity = 1;
        this.price    = price;
    }

    public Product(Product product)
    {
        this.name      = product.name;
        this.model     = product.model;
        this.quantity  = product.quantity;
        this.price     = product.price;
    }

    public String getName() {return name;}

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
