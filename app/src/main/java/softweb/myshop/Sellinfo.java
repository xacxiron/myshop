package softweb.myshop;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.print.PrintManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import softweb.printinginvoice.CustomPrintDocumentAdapter;

public class Sellinfo extends AppCompatActivity
{
    Singleton singleton = Singleton.getInstance();
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sellinfo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.fab);




        final ListView lvSells = (ListView) findViewById(R.id.list);

        double sum=0;
        ArrayList<Sell> s = new ArrayList<>();
        s.add(new Sell(new Product("Item Name","",0,0)));
        for(int i=0;i<singleton.sells.size();i++)
        {
            s.add(singleton.sells.get(i));
            sum += singleton.sells.get(i).get_product().getPrice();
        }
        s.add(new Sell(new Product("Total","",s.size()-1,sum)));

        ListAdapter customAdapter = new ListAdapter(this, s);
        lvSells.setAdapter(customAdapter);


        LayoutAnimationController lac = new LayoutAnimationController(AnimationUtils.loadAnimation(this, R.anim.lv_animation), 0.5f);
        lvSells.setLayoutAnimation(lac);
        //lvSells.setOnItemClickListener(itemClickListener);

        singleton.sells.clear();





        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //doPrint(str);

                // create bitmap screen capture
                View v1 = lvSells;
                v1.setDrawingCacheEnabled(true);
                Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                v1.setDrawingCacheEnabled(false);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                Intent intent = new Intent(getApplicationContext(),PrintPreviewActivity.class);
                intent.putExtra("byte",byteArray);
                startActivity(intent);

            }
        });
    }





    public class ListAdapter extends ArrayAdapter<Sell>
    {
        private final Context context;
        private ArrayList<Sell> itemsArrayList;
        LayoutInflater inflater;



        public ListAdapter(Context context, ArrayList<Sell> itemsArrayList) {

            super(context, -1, itemsArrayList);

            this.context = context;
            this.itemsArrayList = itemsArrayList;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount()
        {
            return this.itemsArrayList.size();
        }

        public Sell getItem(int position)
        {
            return this.itemsArrayList.get(position);
        }

        public long getItemId(int position)
        {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            View rowView = inflater.inflate(R.layout.test, parent, false);

                Typeface light = singleton.getFontFromRes(context, R.raw.light);
                Typeface bold  = singleton.getFontFromRes(context, R.raw.bold);

                TextView pName = (TextView) rowView.findViewById(R.id.p_name);
                pName.setText(itemsArrayList.get(position).get_product().getName());
                pName.setTypeface(light);

                TextView pModel = (TextView) rowView.findViewById(R.id.p_model);
                pModel.setText(itemsArrayList.get(position).get_product().getModel());
                pModel.setTypeface(light);

                TextView pQuantity = (TextView) rowView.findViewById(R.id.p_quantity);
                if(position==0)pQuantity.setText("Quantity");
                    else pQuantity.setText(itemsArrayList.get(position).get_product().getQuantity() + "");
                pQuantity.setTypeface(light);

                TextView pPrice = (TextView) rowView.findViewById(R.id.p_price);
                if(position==0)pPrice.setText("Price");
                else pPrice.setText(itemsArrayList.get(position).get_product().getPrice() + "");
                pPrice.setTypeface(light);

                if(position == 0 || position==itemsArrayList.size()-1)
                {
                    pName.setTypeface(bold);
                    pModel.setTypeface(bold);
                    pQuantity.setTypeface(bold);
                    pPrice.setTypeface(bold);
                }

                LayoutAnimationController lac = new LayoutAnimationController(AnimationUtils.loadAnimation(getContext(), R.anim.lv_animation), 0.5f);
                //0.5f == time between appearance of listview items.
                rowView.setAnimation(lac.getAnimation());

            return rowView;
        }

        public void updateResults(ArrayList<Sell> itemsArrayList)
        {
            this.itemsArrayList = itemsArrayList;
            //Triggers the list update
            notifyDataSetChanged();
        }
    }//




}
