package softweb.printinginvoice;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.pdf.PrintedPdfDocument;
import android.view.Display;
import android.view.WindowManager;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Jobayed Ullah on 11/18/2016.
 */

public class CustomPrintDocumentAdapter extends PrintDocumentAdapter
{
    Context context;
    private int pageHeight;
    private int pageWidth;
    public PdfDocument myPdfDocument;
    public int totalpages = 1;

    private Bitmap docImg;

    public CustomPrintDocumentAdapter(Context context,Bitmap docImg)
    {
        this.context     = context;
        this.docImg = docImg;
    }

    @Override
    public void onLayout(PrintAttributes oldAttributes,
                         PrintAttributes newAttributes,
                         CancellationSignal cancellationSignal,
                         LayoutResultCallback callback,
                         Bundle metadata) {


        myPdfDocument = new PrintedPdfDocument(context, newAttributes);
        pageHeight = 6*72;//newAttributes.getMediaSize().getHeightMils()/1000 * 32;
        pageWidth = 3*72;//newAttributes.getMediaSize().getWidthMils()/1000 * 72;


        if (cancellationSignal.isCanceled() )
        {
            callback.onLayoutCancelled();
            return;
        }

        if (totalpages > 0)
        {
            PrintDocumentInfo.Builder builder = new PrintDocumentInfo
                    .Builder("print_output.pdf")
                    .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                    .setPageCount(totalpages);

            PrintDocumentInfo info = builder.build();
            callback.onLayoutFinished(info, true);
        }
        else
        {
            callback.onLayoutFailed("Page count is zero.");
        }
    }


    @Override
    public void onWrite(final PageRange[] pageRanges,
                        final ParcelFileDescriptor destination,
                        final CancellationSignal cancellationSignal,
                        final WriteResultCallback callback)
    {


        for (int i = 0; i < totalpages; i++) {
            if (pageInRange(pageRanges, i))
            {
                PdfDocument.PageInfo newPage = new PdfDocument.PageInfo.Builder(pageWidth,
                        pageHeight, i).create();

                PdfDocument.Page page =
                        myPdfDocument.startPage(newPage);

                if (cancellationSignal.isCanceled()) {
                    callback.onWriteCancelled();
                    myPdfDocument.close();
                    myPdfDocument = null;
                    return;
                }
                drawPage(page, i);
                myPdfDocument.finishPage(page);
            }
        }

        try {
            myPdfDocument.writeTo(new FileOutputStream(
                    destination.getFileDescriptor()));
        } catch (IOException e) {
            callback.onWriteFailed(e.toString());
            return;
        } finally {
            myPdfDocument.close();
            myPdfDocument = null;
        }

        callback.onWriteFinished(pageRanges);
    }

    private boolean pageInRange(PageRange[] pageRanges, int page)
    {
        for (int i = 0; i<pageRanges.length; i++)
        {
            if ((page >= pageRanges[i].getStart()) && (page <= pageRanges[i].getEnd()))
                return true;
        }
        return false;
    }


    private void drawPage(PdfDocument.Page page, int pagenumber)
    {
        Canvas canvas = page.getCanvas();
                //new Canvas();
                //

        //pagenumber++; // Make sure page numbers start at 1

        int titleBaseLine = 72;
        int leftMargin = 54;

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(20);
        canvas.drawText(
                "MY SHOP" ,
                leftMargin,
                titleBaseLine,
                paint);

        paint.setTextSize(10);
        canvas.drawText("Address: Jacxiron", leftMargin, titleBaseLine + 25, paint);


        PdfDocument.PageInfo pageInfo = page.getInfo();

        int x1 = 20;
        int y1 = titleBaseLine+40;
        int x2 = canvas.getWidth();
        int y2 = canvas.getHeight();

        Rect rectangle = new Rect(x1,y1,x2,y2);
        canvas.drawBitmap(docImg,
                null,
                rectangle,
                null);

    }
}